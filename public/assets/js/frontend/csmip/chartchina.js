define(['jquery', 'bootstrap', 'frontend', 'template', 'form','echarts'],
function ($, undefined, Frontend, Template, Form,echarts) {
   
    var chart = {
        left:function(){
            var myChart = echarts.init(document.getElementById('mainleft'));

            var legendData = [];
            var seriesData = [];
            for(var index in dataList){
                if(dataList[index].value>0){
                    legendData.push(dataList[index].name);
                    seriesData.push(dataList[index]);
                }
            }
            var data = {
                legendData: legendData,
                seriesData: seriesData         
            };

            // var data = {
            //     legendData: ['AA','BB'],
            //     seriesData: [
            //         {name: "AA", value: 11},
            //         {name: "BB", value: 111}
            //     ]          
            // };
            option = {

                tooltip: {
                    triggerOn: "click",
                    formatter: function(e, t, n) {
                        return .5 == e.value ? e.name + "：人数" : e.seriesName + "<br />" + e.name + "：" + e.value
                    }
                },
                legend: {
                    type: 'scroll',
                    orient: 'vertical',
                    right: 10,
                    top: 20,
                    bottom: 20,
                    data: data.legendData
                  },
                  series: [
                    {
                      name: '姓名',
                      type: 'pie',
                      radius: '55%',
                      center: ['40%', '50%'],
                      data: data.seriesData,
                      emphasis: {
                        itemStyle: {
                          shadowBlur: 10,
                          shadowOffsetX: 0,
                          shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                      }
                    }
                  ]
            };
            myChart.setOption(option);           
        }
    };
  
    var Controller = {
        index:function(){
            var that = this;
            chart.left();
            //chart.right();
        }
    };
    return Controller;
});