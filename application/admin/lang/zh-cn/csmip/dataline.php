<?php

return [
    'Id'            => 'id',
    'Csmip_data_id' => '导入IP包',
    'Ip'            => 'IP地址',
    'Country'       => '所属国家',
    'Region'        => '区域',
    'Province'      => '省份',
    'City'          => '城市',
    'Isp'           => 'ISP',
    'Ipregcityid'   => 'IpRegion返回值cityid',
    'Ipregregion'   => 'IpRegion返回值',
    'Createtime'    => '创建时间',
    'Updatetime'    => '更新时间',
    'Status'        => '状态',
    'Admin_id'      => '管理员',
    'B1'            => '备用字段1',
    'B2'            => '备用字段2',
    'B3'            => '备用字段3',
    'B4'            => '备用字段4',
    'B5'            => '备用字段5',
    'B6'            => '备用字段6',
    'B7'            => '备用字段7',
    'B8'            => '备用字段8',
    'B9'            => '备用字段9'
];
