<?php

return [
    'Id'              => 'id',
    'Name'            => '图表名称',
    'Needuserlogin'   => '是否需要登录',
    'Needuserlogin y' => '查看报表需要会员登录',
    'Needuserlogin n' => '查看报表不需要会员登录',
    'Status'          => '状态',
    'Createtime'      => '创建时间',
    'Updatetime'      => '更新时间',
    'Admin_id'        => '管理员',
    'B1'              => '备用字段1',
    'B2'              => '备用字段2',
    'B3'              => '备用字段3',
    'B4'              => '备用字段4',
    'B5'              => '备用字段5',
    'B6'              => '备用字段6',
    'B7'              => '备用字段7',
    'B8'              => '备用字段8',
    'B9'              => '备用字段9'
];
