<?php

return [
    'autoload' => false,
    'hooks' => [
        'app_init' => [
            'alioss',
            'cos',
            'shopro',
        ],
        'module_init' => [
            'alioss',
            'cos',
        ],
        'upload_config_init' => [
            'alioss',
            'cos',
        ],
        'upload_delete' => [
            'alioss',
            'cos',
        ],
        'epay_config_init' => [
            'epay',
        ],
        'addon_action_begin' => [
            'epay',
        ],
        'action_begin' => [
            'epay',
        ],
        'upgrade' => [
            'shopro',
        ],
        'config_init' => [
            'shopro',
            'ueditor',
        ],
    ],
    'route' => [
        '/example$' => 'example/index/index',
        '/example/d/[:name]' => 'example/demo/index',
        '/example/d1/[:name]' => 'example/demo/demo1',
        '/example/d2/[:name]' => 'example/demo/demo2',
    ],
    'priority' => [],
    'domain' => '',
];
