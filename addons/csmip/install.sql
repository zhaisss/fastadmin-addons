
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


 


--
-- 表的结构 `__PREFIX__csmip_data`
--
CREATE TABLE IF NOT EXISTS `__PREFIX__csmip_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(200) NOT NULL COMMENT '批次号',
  `needuserlogin` enum('Y','N') DEFAULT 'Y' COMMENT '是否需要用户登录:Y=需要,N=不需要',
  `chartconfig` text COMMENT '图片配置',
  `status` enum('normal','hidden') NOT NULL DEFAULT 'normal' COMMENT '状态',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员',
  `b1` varchar(100) DEFAULT NULL COMMENT '备用字段1',
  `b2` varchar(100) DEFAULT NULL COMMENT '备用字段2',
  `b3` varchar(100) DEFAULT NULL COMMENT '备用字段3',
  `b4` varchar(100) DEFAULT NULL COMMENT '备用字段4',
  `b5` varchar(100) DEFAULT NULL COMMENT '备用字段5',
  `b6` varchar(100) DEFAULT NULL COMMENT '备用字段6',
  `b7` varchar(100) DEFAULT NULL COMMENT '备用字段7',
  `b8` varchar(100) DEFAULT NULL COMMENT '备用字段8',
  `b9` varchar(100) DEFAULT NULL COMMENT '备用字段9',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8  COMMENT='导入IP包'
;
 



--
-- 表的结构 `__PREFIX__csmip_dataline`
--
CREATE TABLE IF NOT EXISTS `__PREFIX__csmip_dataline` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `csmip_data_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '导入IP包',
  `ip` varchar(100) NOT NULL COMMENT 'IP地址',
  `country` varchar(100) DEFAULT NULL COMMENT '所属国家',
  `region` varchar(100) DEFAULT NULL COMMENT '区域',
  `province` varchar(100) DEFAULT NULL COMMENT '省份',
  `city` varchar(100) DEFAULT NULL COMMENT '城市',
  `isp` varchar(100) DEFAULT NULL COMMENT 'ISP',
  `ipregcityid` int(10) DEFAULT NULL COMMENT 'IpRegion返回值cityid',
  `ipregregion` varchar(200) DEFAULT NULL COMMENT 'IpRegion返回值',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` enum('normal','hidden') DEFAULT 'normal' COMMENT '状态',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员',
  `b1` varchar(100) DEFAULT NULL COMMENT '对应区域',
  `b2` varchar(100) DEFAULT NULL COMMENT '备用字段2',
  `b3` varchar(100) DEFAULT NULL COMMENT '备用字段3',
  `b4` varchar(100) DEFAULT NULL COMMENT '备用字段4',
  `b5` varchar(100) DEFAULT NULL COMMENT '备用字段5',
  `b6` varchar(100) DEFAULT NULL COMMENT '备用字段6',
  `b7` varchar(100) DEFAULT NULL COMMENT '备用字段7',
  `b8` varchar(100) DEFAULT NULL COMMENT '备用字段8',
  `b9` varchar(100) DEFAULT NULL COMMENT '备用字段9',
  PRIMARY KEY (`id`),
  KEY `IX_CSMIP_DATALLINE1` (`csmip_data_id`),
  KEY `IX_CSMIP_DATALLINE2` (`country`),
  KEY `IX_CSMIP_DATALLINE3` (`province`),
  KEY `IX_CSMIP_DATALLINE4` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='IP地址'
;
 

 
COMMIT;