<?php

return [
    [
        'name' => '__tips__',
        'title' => '慎重说明',
        'type' => 'string',
        'content' =>
        array (
        ),
        'value' => '本站点使用了第三方的组件，本插件不对第三方插件负责；本插件的费用不包含第三方的组件费用。',
        'rule' => '',
        'msg' => '',
        'tip' => '',
        'ok' => '',
        'extend' => '',
    ],

];
